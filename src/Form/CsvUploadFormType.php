<?php
declare(strict_types=1);

namespace App\Form;

use App\Validator\CsvHeader;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * @CsvHeader
 */
class CsvUploadFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('csvFile', FileType::class, [
                'label' => 'Ajouter un fichier CSV',
                'required' => true,
                'constraints' => [
                    new File([
                        'maxSize' => '1024k',
                        'mimeTypes' => [
                            'text/csv',
                            'application/vnd.ms-excel',
                            'text/plain'
                        ],
                        'mimeTypesMessage' => 'Veuillez ajouter un fichier CSV valide !',
                    ]),
                    new NotBlank(),
                    //Validate Headers from file
                    new CsvHeader(),
                ],
            ]);
    }
}
