<?php
declare(strict_types=1);

namespace App\Controller;

use App\Form\CsvUploadFormType;
use App\Message\ProcessCsvMessage;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Messenger\MessageBusInterface;

class AdminController extends AbstractController
{
    public function __construct(
        private readonly FormFactoryInterface $formFactory,
        private readonly MessageBusInterface  $messageBus)
    {
    }

    public function index(Request $request)
    {
        $form = $this->formFactory->create(CsvUploadFormType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $csvFile = $form->get('csvFile');
            // Message to process csv and send it by mail
            $message = new ProcessCsvMessage($csvFile->getData()->getPathName());
            $this->messageBus->dispatch($message);
            $this->addFlash('success', 'Csv en cours de traitement, verifier votre boite mail');
        }

        return $this->render('admin/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
