<?php

namespace App\Message;

class ProcessCsvMessage
{
    private $csvFilePath;

    public function __construct(string $csvFilePath)
    {
        $this->csvFilePath = $csvFilePath;
    }

    public function getCsvFilePath(): string
    {
        return $this->csvFilePath;
    }
}