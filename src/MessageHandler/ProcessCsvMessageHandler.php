<?php

namespace App\MessageHandler;

use App\Message\ProcessCsvMessage;
use Symfony\Component\Mailer\Mailer;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Mime\Email;
use Symfony\Component\Mime\Part\DataPart;
use League\Csv\Reader;
use League\Csv\Writer;

#[AsMessageHandler]
class ProcessCsvMessageHandler
{

    public function __construct(
        private readonly MailerInterface $mailer
    )
    {
    }

    public function __invoke(ProcessCsvMessage $message)
    {
        //Read upload CSV
        $csv = Reader::createFromPath($message->getCsvFilePath());
        $csv->setDelimiter(',');
        $csv->setHeaderOffset(0);

        //Create new CSV
        $outputCsv = Writer::createFromFileObject(new \SplTempFileObject());
        $outputCsv->insertOne(['civilite', 'email']);

        foreach ($csv as $record) {
            //Check Mail
            if (filter_var($record['email'], FILTER_VALIDATE_EMAIL)) {
                //Add data to csv
                $civilite = trim($record['prenom']) . ' ' . strtoupper(trim($record['nom']));
                $outputCsv->insertOne([$civilite, $record['email']]);
            }
        }

        // Send Mail
        $email = (new Email())
            ->from('contact@email.com')
            ->to('ath.engels@gmail.com')
            ->subject('CSV Traitement Asynchrone')
            ->text('Veuillez trouver ci-joint le fichier CSV resultant du traitement asynchrone.')
            ->addPart(new DataPart($outputCsv->toString(), 'csv_file.csv'));

        $this->mailer->send($email);
    }
}