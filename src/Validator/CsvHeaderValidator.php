<?php
declare(strict_types=1);

namespace App\Validator;

use Symfony\Component\HttpFoundation\File\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Constraint;

class CsvHeaderValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof CsvHeader) {
            throw new UnexpectedTypeException($constraint, CsvHeader::class);
        }

        if (null === $value || '' === $value) {
            return;
        }
        //return no message if type not correct
        if (!in_array($value->getMimeType(), ['text/csv', 'text/plain', 'application/vnd.ms-excel'])) {
            return;
        }

        $csv = $value->openFile();
        $header = $csv->fgetcsv();

        // Check if the header contains the required column names
        if (!in_array('email', $header) || !in_array('nom', $header) || !in_array('prenom', $header)) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }
}