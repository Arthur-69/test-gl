<?php
declare(strict_types=1);

namespace App\Validator;
use Symfony\Component\Validator\Constraint;

class CsvHeader extends Constraint
{
    public $message = 'Les colonnes requises sont : email, nom, prenom.';
}