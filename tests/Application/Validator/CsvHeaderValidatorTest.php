<?php
declare(strict_types=1);


namespace App\Tests\Application\Validator;

use App\Validator\CsvHeader;
use App\Validator\CsvHeaderValidator;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Test\ConstraintValidatorTestCase;

class CsvHeaderValidatorTest extends ConstraintValidatorTestCase
{
    protected function createValidator(): CsvHeaderValidator
    {
        return new CsvHeaderValidator();
    }

    public function testValidCsvHeader()
    {
        $constraint = new CsvHeader();
        // Valid CSV header
        $csvFile = $this->createCsvFile(['email', 'nom', 'prenom']);
        $this->validator->validate($csvFile, $constraint);
        $this->assertNoViolation();
    }

    public function testInvalidCsvHeader()
    {
        $constraint = new CsvHeader();
        // Invalid CSV header
        $csvFile = $this->createCsvFile(['invalid1', 'invalid2', 'invalid3']);
        $this->validator->validate($csvFile, $constraint);
        $this->buildViolation($constraint->message)
            ->assertRaised();
    }

    private function createCsvFile(array $header)
    {
        $csvFilePath = tempnam(sys_get_temp_dir(), 'csv');
        $csvFile = new \SplFileObject($csvFilePath, 'w');

        // Write the CSV header to the file
        $csvFile->fputcsv($header);

        return new UploadedFile(
            $csvFilePath,
            'test.csv',
            'text/csv',
            null,
            true
        );
    }
}
