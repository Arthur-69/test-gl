<?php
declare(strict_types=1);

namespace App\Tests\Application\Controller;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class AdminControllerTest extends WebTestCase
{
    private function createAuthenticatedClient(): KernelBrowser
    {
        return static::createClient([], [
            'PHP_AUTH_USER' => 'admin',
            'PHP_AUTH_PW' => 'pwd123',
        ]);
    }

    /**
     * @dataProvider csvTestCases
     */
    public function testUploadCsv($fileName, $expectedStatusCode, $expectedMessage)
    {
        $client = $this->createAuthenticatedClient();
        $crawler = $client->request('GET', '/admin');
        if ('' === $fileName)
            $file = '';
        else
            $file = new UploadedFile(__DIR__ . '/../../Data/' . $fileName, $fileName, 'text/csv', null);
        // Get form, add data, and submit it
        $form = $crawler->selectButton('submit')->form();
        $form->setValues([
            'csv_upload_form[csvFile]' => $file,
        ]);
        $client->submit($form);
        // Validate the form data
        $validator = $client->getContainer()->get('validator');
        $formErrors = $validator->validate($form->getPhpValues());

        // Check if there are no form validation errors
        $this->assertCount(0, $formErrors);
        $this->assertEquals($expectedStatusCode, $client->getResponse()->getStatusCode());
        $this->assertStringContainsString($expectedMessage, $client->getResponse()->getContent());
    }

    public function csvTestCases()
    {
        return [
            ['valid.csv', 200, 'Csv en cours de traitement, verifier votre boite mail'],
            ['invalid.csv', 200, 'Les colonnes requises sont : email, nom, prenom.'],
            ['invalid.jpg', 200, 'Veuillez ajouter un fichier CSV valide !'],
            ['', 200, 'This value should not be blank.'],
        ];
    }
}
