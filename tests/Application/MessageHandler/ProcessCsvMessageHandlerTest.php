<?php
declare(strict_types=1);

namespace App\Tests\Application\MessageHandler;

use App\Message\ProcessCsvMessage;
use App\MessageHandler\ProcessCsvMessageHandler;
use League\Csv\Writer;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class ProcessCsvMessageHandlerTest extends WebTestCase
{

    private MailerInterface $mailer;

    protected function setUp(): void
    {
        $this->mailer = $this->createMock(MailerInterface::class);
        $this->handler = new ProcessCsvMessageHandler($this->mailer);
    }

    public function testProcessCsv()
    {
        $csvFilePath = tempnam(sys_get_temp_dir(), 'test_csv');
        $csv = Writer::createFromPath($csvFilePath, 'w+');
        $csv->insertOne(['prenom', 'nom', 'email']);
        $csv->insertOne(['john', 'Doe', 'john.doe@example.com']);
        $csv->insertOne(['jane', 'Smith', 'jane.smith@example']);

        // Create a ProcessCsvMessage with the CSV file path
        $message = new ProcessCsvMessage($csvFilePath);
        // Dispatch the message to the message bus
        $this->getContainer()->get('messenger.default_bus')->dispatch($message);

        /** @var Email $email */
        $email = $this->getMailerMessage(0);
        // Check if mail is ok
        $this->assertInstanceOf(Email::class, $email);
        $this->assertSame('contact@email.com', $email->getFrom()[0]->getAddress());
        $this->assertSame('ath.engels@gmail.com', $email->getTo()[0]->getAddress());
        $this->assertSame('CSV Traitement Asynchrone', $email->getSubject());

        $attachments = $email->getAttachments();
        $this->assertCount(1, $attachments);
        $attachment = $attachments[0];
        $attachmentContents = $attachment->getBody();

        //Check is csv file contains valid data
        $this->assertStringContainsString('john.doe@example.com', $attachmentContents);
        $this->assertStringContainsString('john DOE', $attachmentContents);
        $this->assertStringNotContainsString('jane.smith@example', $attachmentContents);
        $this->assertStringNotContainsString('jane Smith', $attachmentContents);

    }

}
