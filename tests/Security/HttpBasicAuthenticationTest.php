<?php
declare(strict_types=1);


namespace App\Tests\Security;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class HttpBasicAuthenticationTest extends WebTestCase
{
    public function testAuthenticationSuccess()
    {
        $client = static::createClient();

        $client->request('GET', '/admin', [], [], [
            'PHP_AUTH_USER' => 'admin',
            'PHP_AUTH_PW' => 'pwd123',
        ]);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testAuthenticationFailure()
    {
        $client = static::createClient();

        $client->request('GET', '/admin');

        $this->assertEquals(401, $client->getResponse()->getStatusCode());
    }
}
